# Polaris Playground

Repository to hold any tools, files, resources, notebooks, etc, related to the polaris machine learning project.

## Running notebooks

Run inside the main virtual environment:

```bash
# Install dependencies and activate the environment
pipenv sync
pipenv shell

# View notebooks...
jupyter notebook

# ... or run a snippet
python3 snippets/learn_best_features_extraction.py
```

